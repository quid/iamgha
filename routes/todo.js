var express = require('express');
var router = express.Router();

// list todo elements
router.get('/', function(req, res) {
    var todos = req.db.get('todos'); ;
    todos.find({}, function(err, docs) {
        if (err === null) {
            if (docs === null) {
                res.send(404, {status: 'notFound'});
            } else {
                res.render('index', {
                    title: 'ToDos',
                    todos: docs
                });
            }
        } else {
            res.send(500, {status: 'error'});
        }        
    });         
});

// form for adding new todo element
router.get('/new/', function(req, res) {
    
    res.render('new', {
        title: 'Neu',
        today: new Date(),
        //todayPlusOneWeek: date()
    });
});

// get single todo element
router.get('/todo/:id', function(req, res) {
    var todos = req.db.get('todos');
    todos.findById(req.params.id, function(err, doc) {
        res.set('Content-Type', 'application/json');
        if (err === null) {
            if (doc === null) {
                res.send(404, {status: 'notFound'});
            } else {
                res.render('show', {
                    title: doc.name,
                    todo: doc
                });
            }
        } else {
            res.send(500, {status: 'error'});
        }
    });
});

// edit single todo element
router.get('/edit/:id', function(req, res) {
    var todos = req.db.get('todos');
    todos.findById(req.params.id, function(err, doc) {
        res.set('Content-Type', 'application/json');
        if (err === null) {
            if (doc === null) {
                res.send(404, {status: 'notFound'});
            } else {
                res.render('edit', {
                    title: doc.name,
                    todo: doc
                });
            }
        } else {
            res.send(500, {status: 'error'});
        }
    });
});

// insert todo element
router.post('/todo/:id', function(req, res) {
    var todos = req.db.get('todos');
    var newTodo = req.body;
    todos.insert(newTodo, function(err, doc) {
        res.set('Content-Type', 'application/json');
        if (err === null) {
            if (doc === null) {
                res.send(404, {status: 'notFound'});
            }
            res.send({status: 'ok'});
        } else {
            res.send(500, {status: 'error'});
        }        
    }); 
});

// update element
router.put('/todo/:id', function(req, res) {
    var todos = req.db.get('todos');
    var toDoToUpdate = req.body;
    todos.updateById(req.params.id, toDoToUpdate, function(err, doc) {
        res.set('Content-Type', 'application/json');
        if (err === null) {
            if (doc === null) {
                res.send(404, {status: 'notFound'});
            }
            res.send({status: 'ok'});
        } else {
            res.send(500, {status: 'error'});
        }        
    });
});

// delete element
router.delete('/todo/:id', function(req, res) {
    var todos = req.db.get('todos');
    todos.delete(res.params.id, function(err, doc) {
        res.set('Content-Type', 'application/json');
        if (err === null) {
            if (doc === null) {
                res.send(404, {status: 'notFound'});
            }
            res.send({status: 'ok'});
        } else {
            res.send(500, {status: 'error'});
        }        
    }); 
});

module.exports = router;
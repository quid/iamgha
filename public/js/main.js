$(document).ready(function(){
	$('#testform').submit(function(event) {
		event.preventDefault();
		console.log(this);
		var formData = $(this).serialize();
		$.ajax({
			url: 'http://localhost:3000/todo/',
			data: formData,
			method: 'post',
			success: function() {
				window.location.reload();
			}
		});
	});
});

